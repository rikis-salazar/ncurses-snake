#include <unistd.h>
#include "snake.h"

BoardTile::BoardTile( int r, int c ) : row(r), col(c) { }

SnakeGame::SnakeGame() : score(0), delay(500000/*microseconds*/),
        heading(WEST), found_food(false) {
    
    // RNG initialization and setup
    srand( time(NULL));      // <-- Will replace with <random> objects

    
    // ncurses initialization & setup
    initscr();
    nodelay(stdscr,true);    // Snake moves regardless of user input
    keypad(stdscr,true);
    noecho();
    curs_set(0);             // Hides the cursor
    getmaxyx(stdscr, maxheight, maxwidth);

    
    // Initial snake body (configuration might change). 
    for ( int i = 0, r = maxheight/2, c = (maxwidth - INIT_SNAKE_SIZE)/2 ;
          i < INIT_SNAKE_SIZE ; ++i ){
        the_snake.push_back( BoardTile( r, c + i ) );
    }

    // Draw the full body of the snake
    for ( auto& snake_piece : the_snake ){
        move_to(snake_piece);
        if ( snake_piece == snake_head() )
            addch(head_symbol);
        else
            addch(snake_symbol);
    }

    generate_food();
    draw_frame();
    draw_objects();          // Food, and score

    // Here is where the actual drawing takes place.
    refresh();
}


SnakeGame::~SnakeGame() {
    nodelay(stdscr,false);      // get character to exit the game
    getch();
    endwin();
}

void SnakeGame::draw_objects() const{
    // First the food...
    move_to(food_location);
    addch(food_symbol);
 
    // and lastly, the score.
    move(maxheight-1,0);
    printw("Score:\t%d",score);

    return;
}


void SnakeGame::draw_frame() const{
    // Top & bottom
    for ( int i = 0 ; i < maxwidth-1 ; ++i ){
        move(0,i);
        addch(frame_symbol);
        move(maxheight-2,i);
        addch(frame_symbol);
    }
    // Left & right
    for ( int i = 0 ; i < maxheight-1 ; ++i ){
        move(i,0);
        addch(frame_symbol);
        move(i,maxwidth-1);
        addch(frame_symbol);
    }

    return;
}

void SnakeGame::generate_food(){
    bool available_tile = false;

    do {
        // Rows {0, maxheight - 2, maxheight - 1} are taken by the top
        // and bottom of the frame as well as the score info. Similarly,
        // cols {0, maxwidth - 1} are not available. Hence we generate
        // random numbers in the intervals 
        //     [1,maxheight-3] and [1,maxwidth-2]
        // to obtain a possible food tile.
        int r = 1 + rand() % (maxheight - 3) ;
        int c = 1 + rand() % (maxwidth - 2) ;
        BoardTile tile(r,c);

        bool tile_in_snake = false;
        for( auto& snake_piece : the_snake ){
            if ( snake_piece == tile ) {
                tile_in_snake = true;
            }
        }

        if( !tile_in_snake ){
            available_tile = true;
            food_location = tile;  // <-- Uses compiler provided operator=
        }

    } while( !available_tile );

    return;
}


bool SnakeGame::collision(){
    BoardTile head = snake_head();

    // If head at borders return true
    if ( head.row <= 0 || head.row >= maxheight - 2 ||
         head.col <= 0 || head.col >= maxwidth - 1 ) {
        return true;
    }

    // If head inside snake return true
    for ( auto it = the_snake.begin() + 2 ; it != the_snake.end() ; ++it ) {
        if ( *it == head )
            return true;
    }

    // If head gets to food_location update flag
    if ( head == food_location ){
        found_food = true;
        generate_food();
        score += 10;
        draw_objects();

        // Every so often increase difficulty
        if ( (score % PTS_PER_LEVEL) == 0 )
            delay -= 50000;
    }
    else {
        found_food = false;
    }

    // head is in the clear...
    return false;
}

void SnakeGame::move_snake(){
    // Record user key-press and proceed accordingly
    int key = getch();
    BoardTile new_head = snake_head(); // <-- Compiler provided copy constructor

    switch( key ){
        case KEY_LEFT:
            if ( heading != EAST )
                heading = WEST;
            break;

        case KEY_RIGHT:
            if ( heading != WEST )
                heading = EAST;
            break;

        case KEY_UP:
            if ( heading != SOUTH )
                heading = NORTH;
            break;

        case KEY_DOWN:
            if ( heading != NORTH )
                heading = SOUTH;
            break;

        case KEY_BACKSPACE:
            heading = EXIT;
            break;

        default:
            break;
    }

    // Now that we know where we are heading, we move accordingly.
    if ( heading == WEST )
        new_head.col -= 1;
    else if ( heading == EAST )
        new_head.col += 1;
    else if ( heading == NORTH )
        new_head.row -= 1;
    else if ( heading == SOUTH )
        new_head.row += 1;
    else // Do nothing. User wants to quit
        return;



    // Replace old head with new head
    move_to(snake_head());
    addch(snake_symbol);
    move_to(new_head);
    addch(head_symbol);
    the_snake.push_front(new_head);
    
    // If food not found pop_back last snake piece and draw blank space
    // on the tail (end) side of the snake.
    if ( !found_food ){
        move_to( the_snake.back() );
        addch(' ');
        the_snake.pop_back();
    }

    refresh();
    return;
}

void SnakeGame::play_game(){
    do {
        // Game over if collides with non-food item
        int r = maxheight - 1;
        int c = maxwidth/2;    // Arbitrary

        if ( collision() ){
            // Game over, buddy!
            c = (maxwidth - 10)/2;  // length(Game over!) = 10
            move(r,c);
            printw("GAME OVER!");
            heading = EXIT;
        }
        else {
            move_snake();         // Here is where `heading` possibly changes
            usleep(delay);

            if ( heading == EXIT ){
                c = (maxwidth - 9)/2;  // length(Good Bye!) = 9
                move(r,c);
                printw("Good Bye!");
            }

        }

    } while ( heading != EXIT );

    return;
}


inline BoardTile SnakeGame::snake_head() const{
    return the_snake.front();
}


// ************************************************************************
// NON-MEMBERS 
// ************************************************************************
inline void move_to( const BoardTile& b ){
    move(b.row,b.col);
    return;
}

inline bool operator==( const BoardTile& lhs, const BoardTile& rhs ){
    return ( lhs.row == rhs.row && lhs.col == rhs.col );
}

inline bool operator!=( const BoardTile& lhs, const BoardTile& rhs ){
    return !( lhs == rhs );
}
