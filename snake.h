#ifndef SNAKE_H
#define SNAKE_H

#include <iostream>   // usleep?
#include <deque>      
#include <cstdlib>    // rand()?
#include <ncurses.h>


enum direction_type { EXIT, WEST, EAST, NORTH, SOUTH };


struct BoardTile {
    int row;
    int col;

   BoardTile( int r = 0 , int c = 0 );
};


class SnakeGame{
  private:
    static const int INIT_SNAKE_SIZE = 5;
    static const int PTS_PER_LEVEL = 50;
    
    // Characters used to 'draw' the snake
    static const char snake_symbol = 'x'; 
    static const char head_symbol = 'o';
    static const char food_symbol = '*';
    static const char frame_symbol= static_cast<char>(219) ; 

    // Screen dimensions
    int maxwidth;
    int maxheight;

    // Stats and settings
    int score;
    int delay;

    direction_type heading;

    bool found_food;
    BoardTile food_location;

    std::deque<BoardTile> the_snake;


    // Helper functions
    void generate_food();
    bool collision();
    void move_snake();
    void draw_objects() const;
    void draw_frame() const;
    BoardTile snake_head() const;

  public:
    SnakeGame();
    ~SnakeGame();

    void play_game();
};



// For convenience
bool operator==( const BoardTile&, const BoardTile& );
bool operator!=( const BoardTile&, const BoardTile& );
void move_to( const BoardTile& );


#endif
